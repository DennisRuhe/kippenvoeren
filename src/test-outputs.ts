import PiGPIO from './output/PiGPIO';
import { createPiProvider } from './output/pi-provider';
import { getConfiguration } from './util/Config';

function main() {
  const config = getConfiguration();
  const gpio = PiGPIO.fromConfig(config, createPiProvider(config));

  gpio.enableOutputs();
}

main();
