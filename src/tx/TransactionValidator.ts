import { Configuration } from '../util/Config';
import EpochNow from "../util/EpochNow";

export default class TransactionValidator {
  public constructor(
    private data: { [p: string]: any },
    private config: Configuration,
  ) {
  }

  public hasTooManyConfirmations(): boolean {
    return this.data.confirmations > this.config.maxConfirmations;
  }

  public isTooLittle(): boolean {
    return this.getTxAmount() < this.config.minAmount;
  }

  public isTooOld(now: Date = new Date()): boolean {
    return this.getTxAge(now) > this.config.maxAgeSeconds;
  }

  public isAccepted(): boolean {
    return !this.isTooLittle() && !this.isTooOld() && !this.hasTooManyConfirmations();
  }

  public getRejectReason(): string {
    let reason = '';

    if (this.hasTooManyConfirmations()) {
      reason += `\n- Confirmations (Actual > Max): ${this.data.confirmations} > ${this.config.maxConfirmations}`;
    }

    if (this.isTooLittle()) {
      reason += `\n- Amount (Actual < Min): ${this.getTxAmount()} < ${this.config.minAmount}\n`;
    }

    if (this.isTooOld()) {
      reason += `\n- Age (Actual > Max): ${this.getTxAge()} > ${this.config.maxAgeSeconds}\n`;
    }

    return reason;
  }

  private getTxAmount(): number {
    return this.data.details[0].amount;
  }

  private getTxAge(now: Date = new Date()): number {
    return EpochNow(now) - this.data.time;
  }
}
