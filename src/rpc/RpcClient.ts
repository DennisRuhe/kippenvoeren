import BitcoinJsonRpc from 'bitcoin-json-rpc';
import { Configuration } from '../util/Config';

export default class RpcClient {
  public constructor(
    private readonly rpc: BitcoinJsonRpc,
  ) {
  }

  public static fromConfig(config: Configuration): RpcClient {
    return new RpcClient(new BitcoinJsonRpc(config.url));
  }

  public getTransaction(txId: string): Promise<any> {
    return this.rpc.getTransaction(txId);
  }
}
