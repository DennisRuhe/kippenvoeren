import ArgumentParser from './util/ArgumentParser';
import LastRunChecker from './util/LastRunChecker';
import PiGPIO from './output/PiGPIO';
import RpcClient from './rpc/RpcClient';
import TransactionValidator from './tx/TransactionValidator';
import { createPiProvider } from './output/pi-provider';
import { getConfiguration } from './util/Config';

async function main(argv: string[]): Promise<void> {
  let txId: string;

  try {
    txId = ArgumentParser.getTxId(argv);
  } catch (error) {
    console.error('ERROR: No transaction ID found!\n');
    ArgumentParser.printHelpMessage(argv);
    return;
  }

  console.log(`Incoming transaction ${txId}`);
  const config = getConfiguration();
  const lastRunChecker = new LastRunChecker(config);

  if (lastRunChecker.isInCoolDown()) {
    console.log(`Cooldown is active, wait ${lastRunChecker.getSecondsRemaining()} seconds`);
    return;
  }

  const client = RpcClient.fromConfig(config);
  const response: any = await client.getTransaction(txId);
  const validator = new TransactionValidator(response, config);

  if (validator.isAccepted()) {
    console.log(`Accepted transaction ${txId}`);
    lastRunChecker.saveLastRun();

    const gpio = PiGPIO.fromConfig(config, createPiProvider(config));
    gpio.enableOutputs();
  } else {
    console.log(`Rejected transaction ${txId}: ${validator.getRejectReason()}`);
  }
}

main(process.argv);
