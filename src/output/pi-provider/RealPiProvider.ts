import { init } from 'raspi';
import { DigitalOutput } from 'raspi-gpio';
import { PiInitFunction, PiProvider } from './PiProvider';

export default class RealPiProvider implements PiProvider {
  // eslint-disable-next-line class-methods-use-this
  createDigitalOutput(pin: string): DigitalOutput {
    return new DigitalOutput(pin);
  }

  // eslint-disable-next-line class-methods-use-this
  init(cb: PiInitFunction): void {
    // eslint-disable-next-line global-require
    return init(cb);
  }
}
