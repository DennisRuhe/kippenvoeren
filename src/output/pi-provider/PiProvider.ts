import { DigitalOutput } from 'raspi-gpio';

export type PiInitFunction = () => void;

export interface PiProvider {
  init(cb: PiInitFunction): void;
  createDigitalOutput(pin: string): DigitalOutput;
}
