import { Configuration } from '../../util/Config';
import { PiProvider } from './PiProvider';

export function createPiProvider(config: Configuration): PiProvider {
  // eslint-disable-next-line global-require
  const Provider = config.useFakePi ? require('./FakePiProvider').default : require('./RealPiProvider').default;

  return new Provider();
}
