import { DigitalOutput } from 'raspi-gpio';
import { PiInitFunction, PiProvider } from './PiProvider';

export default class FakePiProvider implements PiProvider {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars,class-methods-use-this
  public createDigitalOutput(pin: string): DigitalOutput {
    return {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      write(val: number) {},
      destroy() {},
    } as unknown as DigitalOutput;
  }

  // eslint-disable-next-line class-methods-use-this
  public init(cb: PiInitFunction): void {
    cb();
  }
}
