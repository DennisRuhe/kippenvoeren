import { DigitalOutput } from 'raspi-gpio';
import { Configuration, GPIOConfig } from '../util/Config';
import { PiProvider } from './pi-provider/PiProvider';

interface PinInfo {
  pin: string;
  output: DigitalOutput,
  enableMs: number
}

const HIGH = 1;
const LOW = 0;

export default class PiGPIO {
  public constructor(
    private readonly piProvider: PiProvider,
    private readonly gpio: GPIOConfig,
  ) {
  }

  public static fromConfig(config: Configuration, piProvider: PiProvider): PiGPIO {
    return new PiGPIO(piProvider, config.gpio);
  }

  public enableOutputs(): void {
    this.piProvider.init(() => {
      const pinInfo = this.getPinInfo();
      const maxTimeOut = pinInfo.reduce((acc, cur) => (cur.enableMs > acc ? cur.enableMs : acc), 0);

      const start = Date.now();
      const stop = start + maxTimeOut * 1.25;

      while (stop > Date.now()) {
        const timePassed = Date.now() - start;

        pinInfo.forEach((info) => {
          const enable = !(timePassed > info.enableMs);

          if (enable) {
            info.output.write(HIGH);
          } else {
            info.output.write(LOW);
          }
        });
      }

      pinInfo.forEach((info) => {
        info.output.write(LOW);
        info.output.destroy();
      });
    });
  }

  private getPinInfo(): PinInfo[] {
    return Object.keys(this.gpio)
      .map((k) => ({
        pin: k,
        output: this.piProvider.createDigitalOutput(k),
        enableMs: this.gpio[k],
      }));
  }
}
