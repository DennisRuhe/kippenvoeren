import fs from 'fs';

export interface GPIOConfig {
  [pin: string]: number;
}

export interface Configuration {
  coolDownSeconds: number;
  gpio: GPIOConfig;
  maxAgeSeconds: number;
  maxConfirmations: number;
  minAmount: number;
  url: string;
  useFakePi?: boolean;
}

const DEFAULT_CONFIG_FILE_NAME = 'config.json';

let config: Configuration;

function readConfiguration(configFileName: string): void {
  try {
    const file = fs.readFileSync(configFileName);
    const contents = file.toString();
    config = JSON.parse(contents);
  } catch (error) {
    console.error(`ERROR reading configuration file ${configFileName}. Make sure it exists and it is valid JSON\n`);
    throw error;
  }
}

export function getConfiguration(configFileName = DEFAULT_CONFIG_FILE_NAME): Configuration {
  if (!config) {
    readConfiguration(configFileName);
  }

  return config;
}
