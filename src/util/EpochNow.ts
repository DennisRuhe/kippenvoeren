export default function now(n: Date = new Date()) {
  return Math.floor(n.getTime() / 1000);
}
