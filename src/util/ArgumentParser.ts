export default class ArgumentParser {
  public static getTxId(argv: string[]): string {
    const args = argv.slice(2);

    if (args.length === 0) {
      throw new Error('No arguments provided');
    }

    return args[0];
  }

  public static printHelpMessage(argv: string[]): void {
    const message = `
Usage: ${argv.join(' ')} <txid>

Example: ${argv.join(' ')} 6b13c384c58fec2bca4cad5f6da2ca5cb6dd37ca323525879c53632c80f5b690
`;

    console.log(message);
  }
}
