import fs from 'fs';
import { Configuration } from './Config';
import EpochNow from './EpochNow';

const RADIX = 10;

export default class LastRunChecker {
  private readonly lastRunPath = '.last-run';

  public constructor(
    private readonly config: Configuration,
  ) {
  }

  public readLastRun(): number {
    try {
      const lastRunData = fs.readFileSync(this.lastRunPath).toString();

      return parseInt(lastRunData, RADIX);
    } catch (error) {
      return 0;
    }
  }

  public isInCoolDown(now: Date = new Date()): boolean {
    return this.getSecondsRemaining(now) > 0;
  }

  public saveLastRun(now: Date = new Date()): void {
    fs.writeFileSync(this.lastRunPath, EpochNow(now).toString(RADIX));
  }

  public getSecondsRemaining(now: Date = new Date()): number {
    return this.config.coolDownSeconds - this.getTimeSinceLastRun(now);
  }

  public getTimeSinceLastRun(now: Date = new Date()) {
    const epochNow = EpochNow(now);
    const lastRun = this.readLastRun();

    return epochNow - lastRun;
  }
}
