# Kippenvoeren

Feed your chickens using eGulden.

## Setup

1. Clone this repository on your raspberry pi
2. Make sure you have node / npm installed on your system
3. Run `npm install`
4. Run `npm run build`
5. Setup your eGulden Core `coin.conf` using this sample configuration as a reference. Make sure to edit your password.
    ```
    rpcuser=eguldenrpc
    rpcpassword=password
    rpcallowip=127.0.0.1
    
    server=1
    daemon=1
    
    walletnotify=(cd /path/to/this/repository && npm run start:js "%s")
    ```
6. Copy the config file `config.sample.json` to `config.json`
    - Make sure to change `http://eguldenrpc:password@127.0.0.1:21015` with the username and password from your config

### Running on something other than a raspberry pi

- In step #3 of the setup you can use `npm install -f` to force the installation. Also skip installing any packages
  that may be asked during the installation process.
- Add `"useFakePi": true` in your `config.json` file.
